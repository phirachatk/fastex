import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { BookingPage } from '../pages/booking/booking';
import { BookingPageModule } from '../pages/booking/booking.module';
import { TrackingPage } from '../pages/tracking/tracking';
import { TrackingPageModule } from '../pages/tracking/tracking.module';
import { GmapPage } from '../pages/gmap/gmap';
import { GmapPageModule } from '../pages/gmap/gmap.module';
import { ProfilePage } from '../pages/profile/profile';
import { ProfilePageModule } from '../pages/profile/profile.module';
import { TrackDetPage } from '../pages/track-det/track-det';
import { TrackDetPageModule } from '../pages/track-det/track-det.module';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
  ],
  imports: [
    BrowserModule,
    BookingPageModule,
    TrackingPageModule,
    GmapPageModule,
    ProfilePageModule,
    TrackDetPageModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TrackingPage,
    BookingPage,
    GmapPage,
    ProfilePage,  
    TrackDetPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
