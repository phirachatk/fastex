import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TrackDetPage } from './track-det';

@NgModule({
  declarations: [
    TrackDetPage,
  ],
  imports: [
    IonicPageModule.forChild(TrackDetPage),
  ],
})
export class TrackDetPageModule {}
