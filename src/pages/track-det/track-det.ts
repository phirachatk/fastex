import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ThrowStmt } from '@angular/compiler';

/**
 * Generated class for the TrackDetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-track-det',
  templateUrl: 'track-det.html',
})
export class TrackDetPage {
  id:string;
  sender:string;
  receiver:string;
  weight:string;
  img:string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.sender = this.navParams.get("sender");
    this.receiver = this.navParams.get("receiver");
    this.weight = this.navParams.get("weight");
    this.img = this.navParams.get("img");
    this.id = this. navParams.get("id");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TrackDetPage');
  }

}
