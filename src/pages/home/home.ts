import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { GmapPage } from '../gmap/gmap';
import { BookingPage } from '../booking/booking';
import { TrackingPage } from '../tracking/tracking';
import { ProfilePage } from '../profile/profile';
import { AlertController } from 'ionic-angular';
import { TrackDetPage } from '../track-det/track-det';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  query: string = ''
  db: object[] = [
    {
      id: '00001',
      sender: 'example man',
      receiver: 'example woman',
      weight: 10.5,
      img: 'https://media.tenor.com/images/a202aac67bb673264cbf902b2cfc297b/raw'
    },
    {
      id: '00002',
      sender: 'example man',
      receiver: 'example woman',
      weight: 15.5,
      img: 'http://images2.fanpop.com/image/photos/8600000/random-animals-animals-8676039-1600-1200.jpg'
    },
    {
      id: '00003',
      sender: 'example man',
      receiver: 'example woman',
      weight: 30.5,
      img: 'https://images-na.ssl-images-amazon.com/images/I/51Gji7jFNjL._SX425_.jpg'
    },
  ]

  constructor(public navCtrl: NavController, public alertCtrl: AlertController) {
    
  }

  getItems(ev: any) {
    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.query = val
    }
  }

  onSearch() {
    const data = this.db.filter((item) => item['id'] === this.query)
    if (data.length !== 0) {
      this.toTrackDet(data[0])
    } else {
      this.showAlert("can't find track");
    }
  }
  
  toTrackDet(data:object) {
    this.navCtrl.push(TrackDetPage, data)
  }

  showAlert(mesg: string) {
    const alert = this.alertCtrl.create({
      title: 'Error!',
      subTitle: mesg,
      buttons: ['OK']
    });
    alert.present();
  }

  toGmapPage() {
    this.navCtrl.push(GmapPage);
  }
  toBookingPage() {
    this.navCtrl.push(BookingPage);
  }
  toTrackingPage() {
    this.navCtrl.push(TrackingPage);
  }
  toProfilePage() {
    this.navCtrl.push(ProfilePage);
  }

}
